import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="git-log-edit",
    version="0.0.1",
    author="balki",
    author_email="git-log-edit@balki.me",
    description="Edit git logs",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/balki/git-log-edit",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPLV3 License",
        "Operating System :: OS Independent",
    ],
    entry_points = {
        'console_scripts' : [
             'git-log-edit=git_log_edit.main:main',
            ],
        },
)
