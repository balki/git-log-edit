Edit git history in your favourite editor. You can change name, email, message, time. You can also squash intermediate commits and create new commits.

Warning: Beta software. It may have some bugs but you won't loose your data. 

Installation:

TODO. Provide better instructions. 

You need python3.7, pygit2 and pyyaml

Note: if `pip install pygit2` fails, try the version in Pipfile

---

Screenshot: https://gitlab.com/balki/git-log-edit/blob/master/assets/git-log-edit.png

Help:

    usage: git-log-edit.py [-h] [--repo REPO] [--master MASTER]
                           [--stop_at STOP_AT] [--new_branch_name NEW_BRANCH_NAME]
                           [--ignore_signs]
    
    optional arguments:
      -h, --help            show this help message and exit
      --repo REPO
      --master MASTER
      --stop_at STOP_AT
      --new_branch_name NEW_BRANCH_NAME
      --ignore_signs

---

Workflow:

1. Install dependencies
2. Create symlink or rename the script as `git-log-edit` and place in a directory in your `PATH`.
3. Git commands:

        git checkout my_feature_branch
        #- Do "Edit - compile - Test - commit" cycle
        EDITOR=your-fav-editor git log-edit # EDITOR is optional, vim is used by default.
        #- Delete unwanted commits, fix typhos, add issue ids, fix name/email, Change timestamps so that it doesn't look like you did all the work on the last day
        git checkout commit_id # One printed out by the last command
        git log # Make sure everything is good
        git checkout -b feature_foo
        git push
        #- Create pull request

---

Related useful links: 

* https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History
* https://rtyley.github.io/bfg-repo-cleaner


