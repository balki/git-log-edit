import os
import subprocess
import tempfile
from argparse import ArgumentParser
from dataclasses import dataclass, asdict
from datetime import datetime, timezone, timedelta
from itertools import takewhile
from typing import List, Dict, Iterable

import yaml
from pygit2 import Commit, Repository, Signature


class GitLogEditError(Exception):
    pass


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--repo", default=".")
    parser.add_argument("--master", default='master')
    parser.add_argument("--stop_at")
    parser.add_argument("--new_branch_name")
    parser.add_argument("--ignore_signs", action='store_true')
    return parser.parse_args()


def find_diverge_point(repo: Repository, master: str):
    head_commit: Commit = repo.head.get_object()
    try:
        master_commit: Commit = repo.revparse_single(master)
    except Exception as e:
        raise GitLogEditError(f"Branch {master} invalid. Please pass the branch you want to merge in --master") from e
    return repo.merge_base(head_commit.id, master_commit.id)


def get_log(head_commit: Commit):
    commit = head_commit
    while True:
        yield commit
        parents = commit.parents
        if not parents:
            return
        elif len(parents) != 1:
            raise GitLogEditError(f"Merge commits not supported Commit: {commit.id}")
        commit = commit.parents[0]


def printable_time(time, offset):
    """
    https://www.pygit2.org/recipes/git-show.html
    """
    zone = timezone(timedelta(minutes=offset))
    dt = datetime.fromtimestamp(float(time), zone)
    return dt.isoformat()


def parse_time(date_str):
    dt = datetime.fromisoformat(date_str)
    return int(dt.timestamp()), int(dt.utcoffset().total_seconds() / 60)


def is_same(committer: Signature, author: Signature):
    def as_tuple(sig: Signature):
        return sig.name, sig.email  # , sig.time, sig.offset

    return as_tuple(committer) == as_tuple(author)


@dataclass(frozen=True)
class CommitRep:
    """
    >>> from collections import OrderedDict
    >>> d = asdict(CommitRep( commit="abcd", author="anon", email="anon@anon.com", time="123455", message="msg is fooar", ))
    >>> yaml.dump(OrderedDict(d))
    {}
    """
    commit: str
    author: str
    email: str
    time: str
    message: str


def commit_to_dict(commit: Commit, ignore_signatures=False):
    author: Signature = commit.author
    if (not ignore_signatures) and commit.gpg_signature[0]:
        print(commit.gpg_signature)
        raise GitLogEditError(
            f"Found signed commit {commit.id}.Signed commits will be lost in editing. If that is fine, "
            f"pass --ignore_signs ")

    if not is_same(commit.committer, author):
        raise GitLogEditError(
            f"Commits with different committer and author are not supported. Commit: {commit.id}")

    return asdict(CommitRep(
        commit=str(commit.oid),
        author=author.name,
        email=author.email,
        time=printable_time(author.time, author.offset),
        message=commit.message
    ))


def skip_unchanged(old_items: Iterable, new_items: Iterable):
    """
    >>> list(skip_unchanged("1234", "1287"))
    ['8', '7']
    >>> list(skip_unchanged("1234", "1234"))
    []
    >>> list(skip_unchanged("1234", ""))
    []
    """
    niter = iter(new_items)
    for oe, ne in zip(old_items, niter):
        if oe != ne:
            yield ne
            break
    yield from niter


def setup_yaml():
    """ https://stackoverflow.com/a/8661021 """
    represent_dict_order = lambda self, data: self.represent_mapping('tag:yaml.org,2002:map', data.items())
    yaml.add_representer(dict, represent_dict_order)


setup_yaml()


def edit_commits(commits: List[Dict]):
    with tempfile.NamedTemporaryFile('r+') as f:
        yaml.dump(commits, f, default_flow_style=False)
        f.flush()
        print(f"Creating temporary file {f.name}")
        editor = os.environ.get("EDITOR", "vim")
        subprocess.run([editor, f.name])
        f.seek(0)
        edited_commits = yaml.load(f)
        return list(reversed(list(
            skip_unchanged(reversed(commits), reversed(edited_commits))
        )))


def create_new_history(branch_name, repo: Repository, commits: List[Dict]):
    branch = f"refs/heads/{branch_name}" if branch_name else None

    def get_commit_args():
        for commit_dict in reversed(commits):
            commit_rep = CommitRep(**commit_dict)
            try:
                old_commit: Commit = repo[commit_rep.commit]
            except KeyError as ke:
                raise GitLogEditError("Don't mess with commit ids, you can edit the rest") from ke
            committer = Signature(commit_rep.author, commit_rep.email, *parse_time(commit_rep.time))
            yield old_commit, committer, committer, commit_rep.message, old_commit.tree_id

    first, *rest = list(get_commit_args())
    prev_commit, *args = first
    if not rest:
        repo.create_commit(branch, *args, prev_commit.parent_ids)
        return
    last_id = repo.create_commit(None, *args, prev_commit.parent_ids)
    *rest, last = rest
    for _, *args in rest:
        last_id = repo.create_commit(None, *args, [last_id])
    _, *args = last
    last_id = repo.create_commit(branch, *args, [last_id])
    return last_id


def main():
    args = parse_args()
    repo = Repository(args.repo)
    if args.stop_at:
        stop_at = repo.revparse_single(args.stop_at)
    else:
        stop_at = find_diverge_point(repo, args.master)
    # All commits
    commits = get_log(repo.head.get_object())
    # Required ones
    commits = takewhile((lambda cmt: cmt.id != stop_at), commits)
    commits = [commit_to_dict(commit, args.ignore_signs) for commit in commits]
    if not commits:
        print(f"No commits found")
        return
    edited_commits = edit_commits(commits)
    if not edited_commits:
        print(f"No changes found")
        return
    new_head = create_new_history(args.new_branch_name, repo, edited_commits)
    print(f"Your new head is at {new_head}")
    if args.new_branch_name:
        print(f"Do `git checkout {args.new_branch_name}`")


if __name__ == "__main__":
    main()
